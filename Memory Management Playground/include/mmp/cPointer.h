#ifndef _CPOINTER_H_
#define _CPOINTER_H_

template<bool, typename T = void>
struct TYPEDEF {};
template<typename T>
struct TYPEDEF <true, T> { typedef T TYPE; };


#define TYPE(T)			TYPEDEF<T != void>::TYPE
#define NONVOID(T)		template<typename t = TYPE(T)>


template <typename T>
class cNonVoidTest
{
public:
	NONVOID(T) t someFunc(int value) {}
};

struct blah
{
	int stuff;
};

class cDummy
{
	blah* data;
	size_t size;

public:
	// default constructor
	cDummy();

	// copy constructor
	cDummy(cDummy const& rhs);
	
	// move constructor
	cDummy(cDummy&& rhs);
	
	// param constructor
	cDummy(size_t newSize);

	// destructor
	~cDummy();


	// copy assign
	cDummy& operator =(cDummy const& rhs);
	//cDummy& operator =(cDummy const& rhs) = default;
	//cDummy& operator =(cDummy const& rhs) = delete;
	
	// move assign
	cDummy& operator =(cDummy&& rhs);
	//cDummy& operator =(cDummy&& rhs) = default;
	//cDummy& operator =(cDummy&& rhs) = delete;

	// accessor operator
	blah* operator->() const { return data; }

	

	
};

class cPointer
{

};


// new/delete
void* operator new(size_t sz);
void* operator new[](size_t sz);

void operator delete(void* ptr);
void operator delete[](void* ptr);

// placement new
void* operator new(size_t sz, void* buffer);
void* operator new[](size_t sz, void* buffer);

void operator delete(void* buffer, void* ptr);
void operator delete[](void* buffer, void* ptr);

#include "_inl/cPointer.inl"

#endif // !_CPOINTER_H_