#ifdef _CPOINTER_H
#ifndef _CPOINTER_INL_
#define _CPOINTER_INL_

// pass-thru custom placement new/delete
template <typename bufferType>
inline void* operator new(size_t sz, bufferType* buffer)
{
	// do whatever management buffer does
	//void* ret = buffer->alloc(sz); // literally whatever you want to do
	void* ret = 0;
	return ret;
}

template <typename bufferType>
inline void* operator delete(void *ptr, bufferType* buffer)
{
	//buffer->dealloc(ptr);
}

template <typename bufferType>
inline void* operator new[](size_t sz, bufferType* buffer)
{
	//return operator::new<bufferType>(sz, buffer);
	return 0;
}

template <typename bufferType>
inline void* operator delete[](void* ptr, bufferType* buffer)
{
	//operator::operator delete<bufferType>(ptr, buffer);
}



#endif // !_CPOINTER_INL_
#endif // _CPOINTER_H_