#include "../../../include/mmp/cPointer.h"

//default constructor
cDummy::cDummy()
{
	size = 0;
	data = 0;
}

// copy constructor
cDummy::cDummy(cDummy const& rhs) : cDummy(rhs.size)
{
	// copy data in actual array
	if (data)
	{

	}
}

// move constructor
cDummy::cDummy(cDummy&& rhs)
{
	size = rhs.size;
	data = rhs.data;
	rhs.data = 0;
}

// param constructor
cDummy::cDummy(size_t newSize) : cDummy()
{
	if (newSize > 0)
	{
		size = newSize;
		data = new blah[size];
	}

	else
	{
		size = 0;
		data = 0;
	}
}

// destructor
cDummy::~cDummy()
{
	if (data)
		delete[] data;
}


// copy assign
cDummy& cDummy::operator=(cDummy const& rhs)
{
	if (data)
	{
		size = 0;
		delete[] data;
	}

	if (rhs.data)
	{
		size = rhs.size;
		data = new blah[size];

		// copy data
	}

	return *this;
}

// move assign
cDummy& cDummy::operator=(cDummy&& rhs)
{
	if (data)
	{
		size = 0;
		delete[] data;
	}

	size = rhs.size;
	data = rhs.data;
	rhs.data = 0;

	return *this;
}

// new/delete
void* operator new(size_t sz)
{
	//return ::operator new(sz);
	return 0;
}

void* operator new[](size_t sz)
{
	//return ::operator new(sz);
	return 0;
}

void operator delete(void* ptr)
{
	//::operator delete(ptr);
}
void operator delete[](void* ptr)
{
	//::operator delete(ptr);
}

// placement new
void* operator new(size_t sz, void* buffer)
{
	//::operator new(sz, buffer);
	//return buffer;
	return 0;
}
void* operator new[](size_t sz, void* buffer)
{
	//::operator new(sz, buffer);
	//return buffer;
	return 0;
}

void operator delete(void* buffer, void* ptr)
{

}
void operator delete[](void* buffer, void* ptr)
{

}