/*
Memory Management Playground (MMP)
By Daniel S. Buckstein
Copyright 2019-2020
*/
//-----------------------------------------------------------------------------

#include "../../include/mmp/mmp_memory.h"

#pragma comment(lib, "MemoryManagementPlayground.lib")


//-----------------------------------------------------------------------------

typedef		byte				chunk_kb[1024];


//-----------------------------------------------------------------------------

#define		decl_argc			ui32 const argc
#define		decl_argv			cstrp const argv[]
typedef		i32(*entry_func)(decl_argc, decl_argv);


//-----------------------------------------------------------------------------

int testMMP(decl_argc, decl_argv);
int testMalloc(decl_argc, decl_argv);


//-----------------------------------------------------------------------------

int main(decl_argc, decl_argv)
{
	//return testMMP(argc, argv);
	return testMalloc(argc, argv);
}


//-----------------------------------------------------------------------------

int testMMP(decl_argc, decl_argv)
{
	// stack-allocate a bunch of data
	chunk_kb chunk[12];
	size count = sizeof(chunk);
	ptr chunk_base = mmp_set_zero(chunk, count);

	// call pool init on chunk
	mmp_pool_init(*chunk, count, sizeof(chunk));
	
	// we can then request memory from the chunk
	ptr testBlock = mmp_block_reserve(*chunk, 4);
	ptr testBlock2 = mmp_block_reserve(*chunk, 1024);

	// do whatever we want with testBlock
	// .
	// .
	// .

	// when done, release pool
	mmp_block_release(testBlock, *chunk);
	mmp_block_release(testBlock2, *chunk);

	

	// when we're done with the pool, terminate it
	mmp_pool_term(*chunk);

	// done, stack-allocated data popped
	return 0;
}


//-----------------------------------------------------------------------------

#include <stdlib.h>

int testMalloc(decl_argc, decl_argv)
{
	// read these right to left

	//void* p;
	//int* pi; // pointer to an integer; knows the type of pointer, so it knows how long each byte is
	//int const* kpi;  // pointer to a constant integer; can't change the value of where its pointing; aka int const kpi[]
	//int* const pki; // constant pointer to an integer; can't change where it's pointing;
	//int const* const kpki; // constant pointer to a constant integer; can't change either the value or where its pointing
	//int const* const* const* const kpkpkpki; // constant pointer to a constant pointer to a constant pointer to a constant integer; aka int const kpkpkpki[][][]
	//int**** ppppi; // pointer to a pointer to a pointer to a pointer to an integer; aka int ppppi[][][][]

	// pointers can be used as arrays if needed

	// figure out what this data means
	// implement the data structure as if it were the real thing
	union malloctest
	{
		i32 data[8];
		ptr pdata[8];
		struct
		{
			i32 dummy;
		};

	};
	typedef union malloctest malloctest;

	malloctest* test =  malloc(8);
	malloctest* test2 = malloc(8);
	malloctest* test3 = malloc(8);
	malloctest* test4 = malloc(8);
	malloctest* test5 = malloc(8);
	malloctest* test6 = malloc(8);

	free(test);
	free(test2);
	free(test3);
	free(test4);
	free(test5);
	free(test6);


	// done
	return 0;
}
